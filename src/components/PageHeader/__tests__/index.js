import React from 'react';
import { render } from '@testing-library/react';

import PageHeader from '..';

describe('PageHeader', () => {
  it('renders the passed title and subtitle', () => {
    const { getByText } = render(<PageHeader title="title" subTitle="sub title" />);
    expect(getByText('title')).toBeInTheDocument();
    expect(getByText('sub title')).toBeInTheDocument();
  });
});
