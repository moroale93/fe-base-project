import React from 'react';
import PropTypes from 'prop-types';

export default function PageHeader({ title, subTitle }) {
  return (
    <>
      <h1>{ title }</h1>
      <p>{ subTitle }</p>
    </>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
};
