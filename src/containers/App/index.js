import React from 'react';

import PageHeader from '../../components/PageHeader';

export default function App() {
  return (
    <>
      <div className="bg" />
      <div className="parallax">
        <div className="parallax-group">
          <div className="layer" />
          <div className="layer" />
          <div className="layer" />
          <div className="layer" />
          <div className="layer" />
          <div className="layer fill" />
        </div>
        <div className="content">
          <PageHeader title="Alessandro Moretto" subTitle="GitLab assignment" />
        </div>
      </div>
    </>
  );
}
