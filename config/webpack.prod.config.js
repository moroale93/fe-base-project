const commonWebpackConf = require('./webpack.common.config');

module.exports = Object.assign({}, commonWebpackConf, {
    mode: 'production',
});
