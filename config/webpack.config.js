const commonWebpackConf = require('./webpack.common.config');

module.exports = Object.assign({}, commonWebpackConf, {
    mode: 'development',
    devServer: {
        contentBase: "./dist",
        hot: true,
        inline: true,
        historyApiFallback: true,
        port: 8080,
        open: true
    },
});
