const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const timestamp = new Date().getTime();

module.exports = {
    entry: [
        "./src/index.js"
    ],
    output: {
        path: path.join(__dirname, "../dist"),
        filename: 'main.'+ timestamp + '.js',
        publicPath: '/',
    },
    module: {
        rules:[
            {
                test: /\.js$/,
                exclude: {
                    test: /node_modules/
                },
                loader: 'babel-loader',
                options: {
                    sourceMap: true,
                },
            },
            {
                test: /\.(css|scss)$/i,
                exclude: {
                    test: /node_modules/,
                },
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: { hmr: true },
                    },
                    {
                        loader: 'css-loader',
                        options: { minimize: true },
                    },
                    {
                        loader: 'postcss-loader',
                        options: { plugins: () => [autoprefixer()] },
                    },
                    'sass-loader',
                ],
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].'+ timestamp + '.css',
            chunkFilename: '[id].css'
        })
    ]
};
