module.exports = {
    plugins: [
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-react-jsx',
        '@babel/plugin-proposal-class-properties',
    ],
    presets: [
        [
            '@babel/preset-env',
            {
                corejs: 3,
                useBuiltIns: 'entry',
                targets: {
                    node: 'current',
                },
            },
        ],
        '@babel/preset-react',
    ]
};