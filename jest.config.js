module.exports = {
    roots: ['<rootDir>/src'],
    testPathIgnorePatterns: ['/node_modules/', '/dist/'],
    setupFilesAfterEnv: [
      '@testing-library/jest-dom/extend-expect',
    ]
  };